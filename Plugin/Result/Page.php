<?php
namespace Elogic\Theme\Plugin\Result;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class Page
{
    /**
     * Sticky Header config path
     */
    const STICKY_HEADER_CONFIG_PATH = 'theme_config/header/enable_sticky_header';

    /**
     * Floating header links config path
     */
    const FLOATING_HEADER_LINKS = 'theme_config/mobile_menu/enable_floating_header_links';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Context
     */
    private $context;

    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager
    ) {
        $this->context = $context;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    /**
     * Get is Sticky Header enabled
     *
     * @return bool
     * @throws NoSuchEntityException
     */
    public function isStickyHeaderEnabled() : bool
    {
        return $this->scopeConfig->isSetFlag(
            self::STICKY_HEADER_CONFIG_PATH,
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()
        );
    }

    /**
     * Get is Floating Header Links enabled
     *
     * @return bool
     * @throws NoSuchEntityException
     */
    public function isFloatingHeaderLinks() : bool
    {
        return $this->scopeConfig->isSetFlag(
            self::FLOATING_HEADER_LINKS,
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()
        );
    }

    /**
     * Add body classes
     *
     * @param \Magento\Framework\View\Result\Page $subject
     * @param ResponseInterface $response
     * @return ResponseInterface[]
     * @throws NoSuchEntityException
     */
    public function beforeRenderResult(
        \Magento\Framework\View\Result\Page $subject,
        ResponseInterface $response
    ){

        if ($this->isStickyHeaderEnabled()) {
            $subject->getConfig()->addBodyClass('__has-sticky-header');
        }

        if ($this->isFloatingHeaderLinks()) {
            $subject->getConfig()->addBodyClass('__with-floating-header-links');
        }

        return [$response];
    }
}
