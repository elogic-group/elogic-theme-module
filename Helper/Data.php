<?php
namespace Elogic\Theme\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;


class Data extends AbstractHelper
{
    const MINICART_TYPE_CONFIG_PATH = 'theme_config/checkout/minicart_type';
    const HEADER_PROMOTION_TYPE_CONFIG_PATH = 'theme_config/header/header_promotion';

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager
    ){
        parent::__construct($context);
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    public function getMinicartType()
    {
        return $this->scopeConfig->getValue(
            self::MINICART_TYPE_CONFIG_PATH,
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()
        );
    }

    public function getHeaderPromotion()
    {
        return $this->scopeConfig->getValue(
            self::HEADER_PROMOTION_TYPE_CONFIG_PATH,
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()
        );
    }
}
