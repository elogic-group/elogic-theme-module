<?php

namespace Elogic\Theme\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Elogic\BreezeTheme\Block\Html\ThemeConfigs;

class FloatingMobileLinks implements ArgumentInterface
{
    /**
     * @param ThemeConfigs $themeConfigs
     */
    public function __construct (
        ThemeConfigs $themeConfigs
    ) {
        $this->themeConfigs = $themeConfigs;
    }

    /**
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getIsFloatingHeaderLinks()
    {
        return $this->themeConfigs->isFloatingHeaderLinks();
    }
}
