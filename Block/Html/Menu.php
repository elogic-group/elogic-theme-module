<?php

declare(strict_types=1);

namespace Elogic\Theme\Block\Html;

use Magento\Framework\Api\Search\FilterGroupBuilder;
use Magento\Framework\Api\Search\SearchCriteriaFactory;
use Magento\Framework\Escaper;
use Magento\Framework\Event\Manager as EventManager;
use Magento\Framework\Exception\NoSuchEntityException;
use Snowdog\Menu\Api\MenuRepositoryInterface;
use Snowdog\Menu\Api\NodeRepositoryInterface;
use Snowdog\Menu\Model\Menu\Node\Image\File as ImageFile;
use Snowdog\Menu\Model\NodeTypeProvider;
use Snowdog\Menu\Model\TemplateResolver;
use Magento\Framework\View\Element\Template\Context;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;

class Menu extends \Snowdog\Menu\Block\Menu
{
    /**
     * @var CollectionFactory
     */
    private $_categoryCollectionFactory;

    public function __construct(
        Context $context,
        EventManager $eventManager,
        MenuRepositoryInterface $menuRepository,
        NodeRepositoryInterface $nodeRepository,
        NodeTypeProvider $nodeTypeProvider,
        SearchCriteriaFactory $searchCriteriaFactory,
        FilterGroupBuilder $filterGroupBuilder,
        TemplateResolver $templateResolver,
        ImageFile $imageFile,
        Escaper $escaper,
        CollectionFactory $_categoryCollectionFactory,
        array $data = []
    ) {
        $this->_categoryCollectionFactory =$_categoryCollectionFactory;
        parent::__construct(
            $context,
            $eventManager,
            $menuRepository,
            $nodeRepository,
            $nodeTypeProvider,
            $searchCriteriaFactory,
            $filterGroupBuilder,
            $templateResolver,
            $imageFile,
            $escaper,
            $data
        );
    }

    /**
     * @param $categoryId
     * @return string|null
     * @throws NoSuchEntityException
     */
    public function getCategoryIcon($categoryId): ?string
    {
        $collection = $this->_categoryCollectionFactory->create()
            ->addAttributeToSelect('category_icon')
            ->addIdFilter($categoryId);

        return $this->getCategoryIconUrl($collection);
    }

    /**
     * @param $category
     * @return string|null
     * @throws NoSuchEntityException
     */
    public function getCategoryIconUrl($category): ?string
    {
        if (isset($category->getFirstItem()->getData()['category_icon'])) {
            return $this->_storeManager->getStore()->getBaseUrl() . $category->getFirstItem()->getData()['category_icon'];
        } else return null;
    }
}
