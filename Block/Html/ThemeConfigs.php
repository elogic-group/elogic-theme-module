<?php

namespace Elogic\Theme\Block\Html;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Serialize\SerializerInterface;

class ThemeConfigs extends Template
{
    /**
     * Header Promotion config path
     */
    const HEADER_PROMOTION_CONFIG_PATH = 'theme_config/header/header_promotion';

    /**
     * Additional header links config path
     */
    const HEADER_LINKS_CONFIG_PATH = 'theme_config/header/additional_links';

    /**
     * Login Popup config path
     */
    const LOGIN_POPUP_CONFIG_PATH = 'theme_config/header/enable_login_popup';

    /**
     * Floating Mobile links config path
     */
    const FLOATING_HEADER_LINKS_CONFIG_PATH = 'theme_config/mobile_menu/enable_floating_header_links';

    /**
     * Opening hours config path
     */
    const OPENING_HOURS_CONFIG_PATH = 'theme_config/mobile_menu/opening_hours';

    /**
     * Footer columns config path
     */
    const FOOTER_COLUMNS_CONFIG_PATH = 'theme_config/footer/columns';

    /**
     * Payment methods config path
     */
    const PAYMENT_METHODS_CONFIG_PATH = 'theme_config/footer/payment_methods';

    /**
     * Social media config path
     */
    const SOCIAL_MEDIA_CONFIG_PATH = 'theme_config/social/social_media';

    /**
     * Store number config path
     */
    const STORE_PHONE_NUMBER_PATH = 'general/store_information/phone';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @param Context $context
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     * @param SerializerInterface $serializer
     */
    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        SerializerInterface $serializer
    ) {
        parent::__construct($context);

        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->serializer = $serializer;
    }

    /**
     * Get header promotion
     *
     * @return string|null
     * @throws NoSuchEntityException
     */
    public function getHeaderPromo() : ? string
    {
        $headerPromo = $this->scopeConfig->getValue(
            self::HEADER_PROMOTION_CONFIG_PATH,
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()
        );

        return !is_null($headerPromo) ? $headerPromo : null;
    }

    /**
     * Get mobile links
     *
     * @return array|null
     * @throws NoSuchEntityException
     */
    public function getHeaderLinks() : ? array
    {
        $headerLinks = $this->scopeConfig->getValue(
            self::HEADER_LINKS_CONFIG_PATH,
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()
        );

        return !is_null($headerLinks) ? $this->serializer->unserialize($headerLinks) : null;
    }

    /**
     * Get is Login Popup enabled
     *
     * @return bool
     * @throws NoSuchEntityException
     */
    public function isLoginPopupEnabled() : bool
    {
        return $this->scopeConfig->isSetFlag(
            self::LOGIN_POPUP_CONFIG_PATH,
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()
        );
    }

    /**
     * Get is Floating Header Links enabled
     *
     * @return bool
     * @throws NoSuchEntityException
     */
    public function isFloatingHeaderLinks() : bool
    {
        return $this->scopeConfig->isSetFlag(
            self::FLOATING_HEADER_LINKS_CONFIG_PATH,
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()
        );
    }

    /**
     * Get opening hours
     *
     * @return array|null
     * @throws NoSuchEntityException
     */
    public function getOpeningHours() : ? array
    {
        $openingHours = $this->scopeConfig->getValue(
            self::OPENING_HOURS_CONFIG_PATH,
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()
        );

        return !is_null($openingHours) ? $this->serializer->unserialize($openingHours) : null;
    }

    /**
     * Get footer columns
     *
     * @return array|null
     * @throws NoSuchEntityException
     */
    public function getFooterColumns() : ? array
    {
        $columns = $this->scopeConfig->getValue(
            self::FOOTER_COLUMNS_CONFIG_PATH,
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()
        );

        return !is_null($columns) ? $this->serializer->unserialize($columns) : null;
    }

    /**
     * Get footer links
     *
     * @param int $column
     * @return array|null
     * @throws NoSuchEntityException
     */
    public function getFooterLinks(int $column) : ? array
    {
        $links = $this->scopeConfig->getValue(
            'theme_config/footer/column_'.$column.'_links',
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()
        );

        return !is_null($links) ? $this->serializer->unserialize($links) : null;
    }

    /**
     * Get payment methods
     *
     * @return array|null
     * @throws NoSuchEntityException
     */
    public function getPaymentMethods() : ? array
    {
        $methods = $this->scopeConfig->getValue(
            self::PAYMENT_METHODS_CONFIG_PATH,
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()
        );

        return explode(',', $methods ?? '');
    }

    /**
     * Get social media
     *
     * @return array|null
     * @throws NoSuchEntityException
     */
    public function getSocialMedia() : ? array
    {
        $socialMedia = $this->scopeConfig->getValue(
            self::SOCIAL_MEDIA_CONFIG_PATH,
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()
        );

        return !is_null($socialMedia) ? $this->serializer->unserialize($socialMedia) : null;
    }



    /**
     * Get phone number
     *
     * @return string|null
     * @throws NoSuchEntityException
     */
    public function getPhoneNumber() : ? string
    {
        $phoneNumber = $this->scopeConfig->getValue(
            self::STORE_PHONE_NUMBER_PATH,
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()
        );

        return !is_null($phoneNumber) ? $phoneNumber : null;
    }
}
