<?php
namespace Elogic\Theme\Block\Config;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;

class OpeningHours extends AbstractFieldArray
{
    const DATE = 'label';
    const VALUE = 'value';

    protected function _prepareToRender()
    {
        $this->addColumn(
            self::DATE,
            [
                'label' => __('Day'),
                'class' => 'required-entry'
            ]
        );
        $this->addColumn(
            self::VALUE,
            [
                'label' => __('Opening Hour'),
                'class' => 'required-entry',
            ]
        );
        $this->_addAfter = false;
    }
}
