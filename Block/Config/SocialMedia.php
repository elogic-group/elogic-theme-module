<?php
namespace Elogic\Theme\Block\Config;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;

class SocialMedia extends AbstractFieldArray
{
    const NAME = 'name';
    const LINK = 'link';

    protected function _prepareToRender()
    {
        $this->addColumn(
            self::NAME,
            [
                'label' => __('Social Media'),
                "class" => "required-entry"
            ]
        );
        $this->addColumn(
            self::LINK,
            [
                'label' => __('Link'),
                'class' => 'required-entry',
            ]
        );

        $this->_addAfter = true;
        $this->_addButtonLabel = __('Add Link');
    }
}
