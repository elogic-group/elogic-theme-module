<?php
namespace Elogic\Theme\Block\Config;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;

class Links extends AbstractFieldArray
{
    const LABEL = 'label';
    const LINK = 'link';

    public function _prepareToRender()
    {
        $this->addColumn(
            self::LABEL,
            [
                'label' => __('Label'),
                'class' => 'required-entry'
            ]
        );
        $this->addColumn(
            self::LINK,
            [
                'label' => __('Link'),
                'class' => 'required-entry',
            ]
        );

        $this->_addAfter = true;
        $this->_addButtonLabel = __('Add Link');
    }
}
