<?php
namespace Elogic\Theme\Model\Config\Source;

class MiniCartList implements \Magento\Framework\Data\OptionSourceInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 'dropdown', 'label' => __('Dropdown')],
            ['value' => 'slider', 'label' => __('Pop-Up Slider')]
        ];
    }
}
