<?php

namespace Elogic\Theme\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class PaymentOption implements OptionSourceInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 'jcb', 'label' => __('JCB')],
            ['value' => 'venmo', 'label' => __('Venmo')],
            ['value' => 'discover', 'label' => __('Discover')],
            ['value' => 'dinners-club', 'label' => __('Dinners Club')],
            ['value' => 'apple-pay', 'label' => __('Apple Pay')],
            ['value' => 'amex', 'label' => __('Amex')],
            ['value' => 'visa', 'label' => __('VISA')],
            ['value' => 'mastercard', 'label' => __('Mastercard')],
            ['value' => 'maestro', 'label' => __('Maestro')],
            ['value' => 'paypal', 'label' => __('PayPal')]
        ];
    }
}
