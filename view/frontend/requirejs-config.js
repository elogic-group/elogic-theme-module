/**
 * Elogic Theme - JS config
 */

var config = {
    map: {
        '*': {
            'carousel': 'Elogic_Theme/js/components/slick-carousel',
            'qtyControls': 'Elogic_Theme/js/components/qty-controls',
            'stickyAddToCart': 'Elogic_Theme/js/components/sticky-add-to-cart'
        }
    },

    config: {
        mixins: {
            'mage/collapsible': {
                'Elogic_Theme/js/mixins/collapsible-mixin': true
            },

            'mage/menu': {
                'Elogic_Theme/js/mixins/menu-mixin': true
            }
        }
    },

    deps: [
        'Elogic_Theme/js/components/footer'
    ]
};
