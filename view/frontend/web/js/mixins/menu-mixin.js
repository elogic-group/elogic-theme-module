/**
 * Elogic BreezeTheme - Menu Mixin
 * Adds back button, changes functionality for parent items
 */

define([
    'jquery'
], function($){
    return function() {
        $.widget('mage.elogicMenu', $.mage.menu, {
            _toggleMobileMode: function () {
                $(this.element).off('mouseenter mouseleave');
                this._on({
                    'click .ui-menu-icon': function (event) {
                        event.preventDefault();

                        var parent = $(this)[0].active,
                            dropdown = parent.find('.submenu').eq(0);

                        parent.toggleClass('opened');
                        dropdown.toggleClass('shown').toggle();
                    }
                });

                $('.button-back').each(function () {
                    var parentCategory = $(this).closest('.parent'),
                        parentCategoryName = parentCategory.find('> a').text();

                    $(this).find('.parent-category').text(parentCategoryName);

                    $(this).on('click', function () {
                        var parent = $(this).closest('.parent'),
                            parentDropdown = $(this).parent('ul');

                        parent.removeClass('opened');
                        parentDropdown.removeClass('shown');
                    })
                });
            },
        });

        return $.mage.elogicMenu;
    }
});
