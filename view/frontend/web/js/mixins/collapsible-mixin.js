define([
    'jquery'
], function ($) {
    "use strict";

    var hideProps = {},
        showProps = {};

    hideProps.height = 'hide';
    showProps.height = 'show';

    return function (target) {
        $.widget('mage.collapsible', target, {

            /**
             * Create
             *
             * @private
             */
            _create: function() {
                this.storage = $.localStorage;
                this.icons = false;

                if (typeof this.options.icons === "string") {
                    this.options.icons = $.parseJSON(this.options.icons);
                }

                this._processPanels();
                this._processState();
                this._refresh();

                if (this.options.icons.header && this.options.icons.activeHeader) {
                    this._createIcons();
                    this.icons = true;
                }

                this._bind("click");
                this._trigger("created");
            },

            /**
             * Activate.
             *
             * @return void;
             */
            activate: function () {
                if (this.options.disabled) {
                    return;
                }

                if (this.options.animate) {
                    this._animate(showProps);
                } else {
                    this.content.fadeIn(300);
                }
                this._open();
            }

        });
        return $.mage.collapsible;
    }
});
