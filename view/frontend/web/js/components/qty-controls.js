/*
 * Elogic Default - Qty Controls
 * Adds Qty Controls
 */

define([
    'jquery'
], function ($) {
    'use strict';

    $.widget('elogic.qtyControls', {

        /**
         * Create function
         *
         * @private
         */
        _create: function() {
            this._initControls();
        },

        /**
         * Init controls
         *
         * @private
         */
        _initControls: function() {
            var self = this,
                el = $(self.element);

            el.find('.qty-control').on('click', function(e) {
                e.preventDefault();

                var iterator = $(this),
                    qtyInput = el.find('input.qty'),
                    currentQty = qtyInput.val(),
                    iteratorType = iterator.data('type');

                    if (iteratorType === "decrement" && currentQty > 1) {
                        qtyInput.val(parseInt(currentQty) - 1);
                    }

                    if (iteratorType === "increment") {
                        qtyInput.val(parseInt(currentQty) + 1);
                    }
            });
        }
    });

    return $.elogic.qtyControls;
});
