/**
 * Elogic BreezeTheme - Slick carousel
 * Carousel functionality
 */

define([
    'jquery',
    'slick'
], function ($) {
    'use strict';

    $.widget('elogic.carousel', {

        /**
         * Default values
         */
        options: {
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            fade: false,
            centerMode: false,
            progressBar: true,
            slidesCount: false,
            infinite: true,
            responsive: [
                {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        centerMode: true
                    }
                }
            ]
        },

        /**
         * Create function
         *
         * @private
         */
        _create: function() {
            var opts = this.options,
                slider = $(this.element),
                progressBar = '.progress-bar',
                progressBarHtml = '<div class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>';

            if (opts.progressBar) {
                slider.parent().append(progressBarHtml);

                slider.on('init reInit', function(event, slick) {
                    var calc = 100 / slick.slideCount;

                    $(progressBar).css('background-size', calc + '% 100%').attr('aria-valuenow', calc);
                });

                slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
                    var calc = ((nextSlide+1) / (slick.slideCount)) * 100;

                    $(progressBar).css('background-size', calc + '% 100%').attr('aria-valuenow', calc);
                });
            }

            if (opts.slidesCount) {
                slider.parent().append(
                    '<div class="slick-info">' +
                        '<span class="current-slide"></span>' +
                        '<span class="dash">/</span>' +
                        '<span class="slides-count"></span>' +
                    '</div>');

                slider.on('init reInit afterChange', function (event, slick, currentSlide) {
                    var i = (currentSlide ? currentSlide : 0) + 1,
                        slideCount = slick.slideCount == null ? 3 : slick.slideCount;

                    $('.current-slide').text(i);
                    $('.slides-count').text(slideCount);
                });
            }

            slider.slick({
                slidesToShow: opts.slidesToShow,
                slidesToScroll: opts.slidesToScroll,
                arrows: opts.arrows,
                dots: opts.dots,
                centerMode: opts.centerMode,
                fade: opts.fade,
                responsive: opts.responsive,
                infinite: opts.infinite
            });

            window.addEventListener('resize', function(event) {
                slider.slick('resize');
            }, true);
        }
    });

    return $.elogic.carousel;
});
