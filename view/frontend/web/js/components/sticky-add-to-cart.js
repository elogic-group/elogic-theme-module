/*
 * Elogic Theme - Sticky add to cart button
 * Copies add to cart box and makes it sticky on mobile
 */

define([
    'jquery',
    'matchMedia'
], function ($, mediaCheck) {
    'use strict';

    $.widget('elogic.stickyAddToCart', {

        options: {
            pageProductConfigurable: 'page-product-configurable',
            formContainer: '.product-info-main',
            addToCart: '.box-tocart',
            stickyAddToCart: '.sticky-addto',
            addToButton: '#product-addtocart-button',
            productAddToInput: '.product-info-main .box-tocart #qty',
            productAddToQty: '.product-info-main .box-tocart .qty-control'
        },

        /**
         * Create function
         *
         * @private
         */
        _create: function() {
            var self = this,
                opts = this.options;

            mediaCheck({
                media: '(max-width: 768px)',
                entry: function () {
                    self._cloneAddToCart();
                    self._triggerAddToCart();
                    self._showHideStickyBox();
                },
                exit: function () {
                    $(opts.stickyAddToCart).remove();
                }
            });
        },

        /**
         * Clone add to cart box
         */
        _cloneAddToCart: function () {
            var opts = this.options,
                stickyAddToCart = $(opts.addToCart).clone();

            $(opts.formContainer).append(stickyAddToCart);
            stickyAddToCart.addClass('sticky-addto');
        },

        /**
         * Check if add to cart block is visible on page scroll
         */
        _isScrolledIntoView: function() {
            var opts = this.options,
                addToCart = $(opts.addToCart);

            var docViewTop = $(window).scrollTop(),
                docViewBottom = docViewTop + $(window).height(),
                elTop = addToCart.offset().top,
                elBottom = elTop + addToCart.height();

            return ((elBottom <= docViewBottom) && (elTop >= docViewTop));
        },

        /**
         * Show/Hide sticky add to cart box
         */
        _showHideStickyBox: function () {
            var self = this,
                opts = this.options;

            window.addEventListener('scroll', function() {
                if (!self._isScrolledIntoView()) {
                    $(opts.stickyAddToCart).slideDown();
                } else {
                    $(opts.stickyAddToCart).slideUp();
                }
            });
        },

        /**
         * Trigger initial add to cart button
         * (ajax is not working on the second button)
         */
        _triggerAddToCart: function () {
            var opts = this.options,
                button = $(opts.stickyAddToCart).find(opts.addToButton),
                stickyAddToInput =  $(opts.stickyAddToCart).find('input.qty'),
                productAddToInput = $(opts.productAddToInput),
                productAddToQty = $(opts.productAddToQty);

            productAddToQty.on('click', function (e) {
                stickyAddToInput.val($(productAddToInput).val());
            });

            this._triggerQtyButtons();

            button.on('click', function (e) {
                e.preventDefault();
                $(opts.addToButton).eq(0).trigger('click');
            });
        },

        /**
         * Trigger qty buttons in sticky add to cart
         */
        _triggerQtyButtons: function () {
            var opts = this.options,
                productAddToInput = $(opts.productAddToInput);

            $(opts.stickyAddToCart).find('.qty-control').on('click', function(e) {
                e.preventDefault();

                var iterator = $(this),
                    qtyInput =  $(opts.stickyAddToCart).find('input.qty'),
                    currentQty = qtyInput.val(),
                    iteratorType = iterator.data('type');

                if (iteratorType === "decrement" && currentQty > 1) {
                    qtyInput.val(parseInt(currentQty) - 1);
                }

                if (iteratorType === "increment") {
                    qtyInput.val(parseInt(currentQty) + 1);
                }

                $(productAddToInput).val(qtyInput.val());
            });
        }
    });

    return $.elogic.stickyAddToCart;
});
