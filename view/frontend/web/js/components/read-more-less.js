/*
 * Elogic Default - Read More/Less
 * Read More/Less functionality
 */

define([
    'jquery',
    'Elogic_Theme/js/libraries/read-more.min',
    'mage/translate'
], function ($, $readMoreJS, $t) {
    'use strict';

    $.widget('elogic.readMoreLess', {

        /**
         * Default values
         */
        options: {
            targetElement: '.read-more',
            wordsCount: 80,
            toggle: true,
            moreLink: $t('Read More'),
            lessLink: $t('Read Less'),
            linkClass: 'action primary'
        },

        /**
         * Create function
         *
         * @private
         */
        _create: function() {
            var opts = this.options;

            $readMoreJS({
                target: opts.targetElement,
                wordsCount: opts.wordsCount,
                toggle: opts.toggle,
                moreLink: opts.moreLink,
                lessLink: opts.lessLink,
                linkClass: opts.linkClass,
            });
        }
    });

    return $.elogic.readMoreLess;
});
