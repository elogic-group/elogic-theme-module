<?php

declare(strict_types=1);

namespace Elogic\Theme\Setup\Patch\Data;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class AddAdditionalProductAttribute implements \Magento\Framework\Setup\Patch\DataPatchInterface
{
    private const ATTRIBUTE_NAME = 'product_additional';

    /**
     * @param EavSetupFactory $eavSetupFactory
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        protected EavSetupFactory $eavSetupFactory,
        protected ModuleDataSetupInterface $moduleDataSetup
    ){
    }

    /**
     * @return void
     * @throws LocalizedException
     * @throws \Zend_Validate_Exception
     */
    public function apply(): void
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            self::ATTRIBUTE_NAME,
            [
                'type' => 'text',
                'label' => 'Additional Information',
                'input' => 'textarea',
                'required' => false,
                'sort_order' => 228,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'Product Additional Information',
            ]
        );

        $eavSetup->updateAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            self::ATTRIBUTE_NAME,
            [
                'is_pagebuilder_enabled' => 1,
                'is_html_allowed_on_front' => 1,
                'is_wysiwyg_enabled' => 1
            ]
        );

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases(): array
    {
        return [];
    }
}
